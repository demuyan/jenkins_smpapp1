package com.example.sample1;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.app.Activity;

public class MainActivity extends Activity {

    private EditText et1;
    private EditText et2;
    private Button btn1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText)findViewById(R.id.et1);
        et2 = (EditText)findViewById(R.id.et2);
        btn1 = (Button)findViewById(R.id.btn1);

        // 押下時
        btn1.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                // 取得
                String s = et1.getText().toString();

                // 加工
                s = addHello(s);

                // 表示
                et2.setText(s);
            }
       });
    }


    // 加工
    public String addHello(String s) {
        return "Hello, " + s + "!";
    }
}